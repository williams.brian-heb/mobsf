# MobSF Analyzer

The MobSF analyzer is a command line tool that wraps [the Mobile Security Framework (MobSF)](https://github.com/MobSF/Mobile-Security-Framework-MobSF).
It provides capabilities for scanning iOS and Android applications using [GitLab SAST](https://docs.gitlab.com/ee/user/application_security/sast/).

## How it works

The analyzer uses the [GitLab Analyzers Common Library](https://gitlab.com/gitlab-org/security-products/analyzers/common) as an application framework.

An analysis run has three stages:
1. Match - The analyzer recursively searches the target directory for a "match" which indicates that analysis should be run on the project. The MobSF analyzer will run if the project contains an `.xcodeproj` directory, or an `AndroidManifest.xml` file. It will give up on searching once it reaches `SEARCH_MAX_DEPTH`.
2. Analyze - The analyzer will perform analysis of project source code. The MobSF analyzer does this by zipping the project source code and uploading it to the MobSF service defined by `MOBSF_ADDR`. By default, it expects the MobSF service to be running in a sidecar container at `http://mobsf:8000`. Once analysis is completed, the scan report is passed to the Convert stage.
3. Convert - The MobSF scan report is converted into [GitLab's JSON Report format](https://docs.gitlab.com/ee/user/application_security/sast/#reports-json-format). The resulting artifact is stored in the pipeline artifacts as `gl-sast-report.json`. GitLab is able to display findings on the merge request by parsing this artifact.

## Building

The MobSF analyzer is packaged as a Docker container. To build it, you need to have docker installed.

You can build using the build script:

```
$ ./scripts/build.sh
Building: registry.gitlab.com/gitlab-org/security-products/analyzers/mobsf:90d086a8a0c45896c3d4cea1d68ad0a4b7a80a12
Sending build context to Docker daemon  7.311MB
Step 1/7 : FROM golang:1.15 as build
 ---> 9f495162f677
[snip]
Successfully built 5125e52b5f6f
Successfully tagged registry.gitlab.com/gitlab-org/security-products/analyzers/mobsf:90d086a8a0c45896c3d4cea1d68ad0a4b7a80a12
```

## Running it locally

The MobSF analyzer has the MobSF service as a dependency, so two containers need to be run.

1. Start the MobSF service

    ```
    docker run --rm -d \
      -p 8000:8000 \
      -e "MOBSF_API_KEY=key" \
      --name mobsf \
      --net=bridge \
      opensecurity/mobile-security-framework-mobsf:latest
    ```

2. Get the ip of the MobSF container

    ```
    docker inspect -f '{{.NetworkSettings.IPAddress}}' mobsf
    ```

3. Run the analyzer (while within the target project directory)

    ```
    docker run --rm -ti \
      -v "$(pwd):/target" \
      -e ANALYZER_TARGET_DIR=/target/ \
      -e SEARCH_MAX_DEPTH=4 \
      -e MOBSF_ADDR=http://<mobsf_container_ip>:8000/api/v1 \
      -e MOBSF_API_KEY=key \
      mobsf:latest
    ```

## Running in GitLab CI

You can easily add the analyzer to a GitLab CI job by `include`ing the [ci template](template/mobsf.gitlab-ci.yml).

Here is an example:

```yml
include:
  - project: 'gitlab-org/security-products/analyzers/mobsf'
    ref: master
    file: '/template/mobsf.gitlab-ci.yml'
```
