#!/bin/sh

display_help() {
    echo "Usage: $0 [--publish]"
    echo "       --publish       Push the image to the registry"
    echo
    echo "Builds a docker image, and optionally pushes it"
}

PUBLISH=0

while [ "$1" != "" ]; do
 case $1 in
   --publish )
                      PUBLISH=1
                      ;;
   * )                echo "$0: Invalid argument $1"
                      display_help
                      exit 1
   esac
   shift
done

cd "$(dirname "$0")/.." || exit 1

tag="${CI_COMMIT_SHA:-$(git rev-parse HEAD)}"
image_name="registry.gitlab.com/gitlab-org/security-products/analyzers/mobsf"
image_full_name="${image_name}:${tag}"
echo "Building: ${image_full_name}"
docker build -t "$image_full_name" . || exit 1

if [ "$PUBLISH" != 0 ]; then
  docker push "$image_full_name"
fi

if [ "$CI_COMMIT_BRANCH" = master ]; then
  latest="${image_name}:latest"
  docker tag "$image_full_name" "$latest"
  if [ "$PUBLISH" != 0 ]; then
    docker push "$latest"
  fi
fi
