package main

import (
	"encoding/json"
	"io"
	"mobsf/metadata"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	report := new(Report)
	if err := json.NewDecoder(reader).Decode(report); err != nil {
		return nil, err
	}

	issues := []issue.Issue{}
	for _, f := range report.Findings() {
		// The info findings are extremely high-volume and low-value.
		// So, we won't report them.
		if f.SeverityLevel() <= issue.SeverityLevelInfo {
			continue
		}

		for _, loc := range f.Locations {
			i := issue.Issue{
				Category:    issue.CategorySast,
				Scanner:     metadata.IssueScanner,
				Name:        f.ID,
				Message:     f.Description,
				Description: f.Description,
				Severity:    f.SeverityLevel(),
				Location:    loc.Location(),
			}
			issues = append(issues, i)
		}
	}

	r := issue.NewReport()
	r.Vulnerabilities = issues
	r.Scan.Scanner = metadata.ReportScanner
	return &r, nil
}
