package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if the file is AndroidManifest.xml or *.xcodeproj
func Match(path string, info os.FileInfo) (bool, error) {
	filename := info.Name()
	if filepath.Ext(filename) == ".xcodeproj" {
		return true, nil
	}

	if filename == "AndroidManifest.xml" {
		return true, nil
	}

	return false, nil
}

func init() {
	plugin.Register("mobsf", Match)
}
